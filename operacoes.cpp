#include <stdio.h>
#include <stdlib.h>

int pos=0;
char exp[20];

/* Exp -> dig R     {Exp.val = dig.val + R.val}
   R -> + dig R1 |   {R.val = dig.val + R1.val}
   vazio        {R.val = 0} */
int T (int *Tval);
int S (int *Ss, int Sh);
int F (int *Fval);
int R (int Rh, int *Rs);
int E (int *Eval);
int Num(int *NumVal);
int RN(int *RNs, int RNh);

int T (int *Tval){
    int Sh, Ss, Fval;

    if (F(&Fval)){
        Sh = Fval;
        if (S(&Ss, Sh)){
            *Tval = Ss;
            return 1;
        }
        return 0;
    }
    return 0;
}

int S (int *Ss, int Sh){
    int Fval, S1s, S1h;

    if (exp[pos]=='*'){
        pos++;
        if (F(&Fval)){
            S1h = Sh * Fval;
            if (S(&S1s, S1h)){
                *Ss = S1s;
                return 1;
            }
            return 0;
        }
        return 0;
    }
    else if (exp[pos]=='/'){
        pos++;
        if (F(&Fval)){
            S1h = Sh / Fval;
            if (S(&S1s, S1h)){
                *Ss = S1s;
                return 1;
            }
            return 0;
        }
        return 0;
    }
    else{
        *Ss = Sh;
        return 1;
    }
}

int F (int *Fval){
    int Eval, NumVal;
    if (Num(&NumVal)){
        *Fval = NumVal;
        return 1;
    }
    if (exp[pos]=='('){
        pos++;
        if (E(&Eval)){
            *Fval = Eval;
            if (exp[pos]==')'){
                pos++;
                return 1;
            }
            return 0;
        }
        return 0;
    }

    return 0;
}

int Num(int *NumVal){
    int RNh, RNs;
    if (exp[pos]>='0' && exp[pos]<='9'){
        RNh = exp[pos]-'0';
        pos++;
        if (RN(&RNs, RNh)){
            *NumVal = RNs;
            return 1;
        }
        return 0;
    }
    return 0;
}

int RN(int *RNs, int RNh){
    int RN1h, RN1s;
    if (exp[pos]>='0' && exp[pos]<='9'){
        RN1h = RNh * 10 + exp[pos]-'0';
        pos++;
        if (RN(&RN1s, RN1h)){
            *RNs = RN1s;
            return 1;
        }
        return 0;
    }
    else {
        *RNs = RNh;
        return 1;
    }
}

int R (int Rh, int *Rs)
{
    int Tval, R1s, R1h;
    if (exp[pos]=='+')
    {
        pos++;
        if (T(&Tval))
        {
            R1h = Rh + Tval;
            if (R(R1h, &R1s)) 
            {
                *Rs=R1s;
                return 1;
            }
            return 0;
        }
    }
    else if (exp[pos]=='-')
    {
        pos++;
        if (T(&Tval))
        {
            R1h = Rh - Tval;
            if (R(R1h, &R1s)) 
            {
                *Rs=R1s;
                return 1;
            }
            return 0;
        }
    }
    else 
    {
        *Rs=Rh;
        return 1;
    }
} 

int E (int *Eval)
{
    int Rs, Rh, Tval;
    if (T(&Tval))
    {
        Rh = Tval;
        if (R(Rh, &Rs)) 
        {
            *Eval = Rs;
            return 1;
        }
        return 0;
    }
    else return 0;
} 

int main()
{
    int valor;
    printf("Entre com a express�o a avaliar:");
    fflush(stdin);
    scanf("%s",exp);
    if (E(&valor))
        printf("A express�o vale %d\n",valor);
    else printf("Express�o inv�lida");
    system("pause");   
}

