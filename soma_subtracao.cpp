#include <stdio.h>
#include <stdlib.h>

int pos=0;
char exp[20];

/* Exp -> dig R     {Exp.val = dig.val + R.val}
   R -> + dig R1 |   {R.val = dig.val + R1.val}
        vazio        {R.val = 0} */
   
int R (int Rh, int *Rs)
{
    int Tval, R1s, R1h;
    if (exp[pos]=='+')
    {
        pos++;
        if (exp[pos]>='0' && exp[pos]<='9')
        {
            Tval=exp[pos]-'0';
            pos++;
            R1h = Rh + Tval;
            if (R(R1h, &R1s)) 
            {
                *Rs=R1s;
                return 1;
            }
            return 0;
        }
    }
    else if (exp[pos]=='-')
    {
        pos++;
        if (exp[pos]>='0' && exp[pos]<='9')
        {
            Tval=exp[pos]-'0';
            pos++;
            R1h = Rh - Tval;
            if (R(R1h, &R1s)) 
            {
                *Rs=R1s;
                return 1;
            }
            return 0;
        }
    }
    else 
    {
        *Rs=Rh;
        return 1;
    }
} 

int E (int *Eval)
{
    int Rs, Rh, Tval;
    if (exp[pos]>='0' && exp[pos]<='9')
    {
        Tval=exp[pos]-'0';
        pos++;
        Rh = Tval;
        if (R(Rh, &Rs)) 
        {
            *Eval = Rs;
          return 1;
        }
       return 0;
       }
    else return 0;
} 

int main()
{
    int valor;
    printf("Entre com a express�o a avaliar:");
    fflush(stdin);
    scanf("%s",exp);
    if (E(&valor))
        printf("A express�o vale %d\n",valor);
    else printf("Express�o inv�lida");
    system("pause");   
}

