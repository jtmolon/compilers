#include <stdio.h>
#include <stdlib.h>

int pos=0;
char exp[20];

/* Exp -> dig R     {Exp.val = dig.val + R.val}
   R -> + dig R1 |   {R.val = dig.val + R1.val}
        vazio        {R.val = 0} */
   
int R (int *Rval)
{
int digval, R1val;
if (exp[pos]=='+')
   {
   pos++;
   if (exp[pos]>='0' && exp[pos]<='9')
      {
      digval=exp[pos]-'0';
      pos++;
      if (R(&R1val)) 
         {
         *Rval=digval+R1val;
         return 1;
         }
      return 0;
      }
   }
else 
   {
   *Rval=0;
   return 1;
   }
} 

int Exp (int *eval)
{
int Rval,digval;
if (exp[pos]>='0' && exp[pos]<='9')
   {
   digval=exp[pos]-'0';
   pos++;
   if (R(&Rval)) 
      {
      *eval=digval+Rval;
      return 1;
      }
   return 0;
   }
else return 0;
} 

int main()
{
int valor;
printf("Entre com a express�o a avaliar:");
fflush(stdin);
scanf("%s",exp);
if (Exp(&valor))
   printf("A express�o vale %d\n",valor);
else printf("Express�o inv�lida");
system("pause");   
}

