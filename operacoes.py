# -*- coding: utf-8 -*-

class Operacoes(object):
    
    def __init__(self):
        self.pos = 0
        self.exp = ""
        self.main()

    def T(self, Tval = [None,]):
        Sh = [None,]
        Ss = [None,]
        Fval = [None,]
        
        if self.F(Fval):
            Sh[0] = Fval[0]
            if self.S(Ss, Sh):
                Tval[0] = Ss[0]
                return 1        
            return 0 
        return 0 

    def S(self, Ss = [None,], Sh = [None,]):
        Fval = [None,]
        S1s = [None,]
        S1h = [None,]

        if self.exp[self.pos] == '*':
            self.pos += 1
            if self.F(Fval):
                S1h[0] = Sh[0] * Fval[0]
                if self.S(S1s, S1h):
                    Ss[0] = S1s[0]
                    return 1                
                return 0
            return 0
        elif self.exp[self.pos] == '/':
            self.pos += 1
            if self.F(Fval):
                S1h[0] = Sh[0] / Fval[0]
                if self.S(S1s, S1h):
                    Ss[0] = S1s[0]
                    return 1                
                return 0
            return 0
        else:
            Ss[0] = Sh[0]
            return 1

    def F(self, Fval = [None,]):
        Eval = [None,]
        NumVal = [None,]
        if self.Num(NumVal):
            Fval[0] = NumVal[0]
            return 1
        if self.exp[self.pos]=='(':
            self.pos += 1
            if self.E(Eval):
                Fval[0] = Eval[0]
                if self.exp[self.pos]==')':
                    self.pos += 1
                    return 1
                return 0
            return 0
        return 0

    def Num(self, NumVal = [None,]):
        RNh = [None,]
        RNs = [None,]
        if self.exp[self.pos] >= '0' and self.exp[self.pos] <= '9':
            RNh[0] = int(self.exp[self.pos])
            self.pos += 1
            if self.RN(RNs, RNh):
                NumVal[0] = RNs[0]
                return 1
            return 0
        return 0

    def RN(self, RNs = [None,], RNh = [None,]):
        RN1h = [None,]
        RN1s = [None,]
        if self.exp[self.pos]>='0' and self.exp[self.pos]<='9':
            RN1h[0] = RNh[0] * 10 + int(self.exp[self.pos])
            self.pos += 1
            if self.RN(RN1s, RN1h):
                RNs[0] = RN1s[0]
                return 1
            return 0
        else:
            RNs[0] = RNh[0]
            return 1

    def R(self, Rh = [None,], Rs = [None,]):
        Tval = [None,]
        R1s = [None,]
        R1h = [None,]
        if self.exp[self.pos] == '+':
            self.pos += 1
            if self.T(Tval):
                R1h[0] = Rh[0] + Tval[0]
                if self.R(R1h, R1s):
                    Rs[0]=R1s[0]
                    return 1
                return 0
        elif self.exp[self.pos]=='-':
            self.pos += 1
            if self.T(Tval):
                R1h[0] = Rh[0] - Tval[0]
                if self.R(R1h, R1s):
                    Rs[0]=R1s[0]
                    return 1
                return 0
        else:
            Rs[0]=Rh[0]
            return 1

    def E(self, Eval = [None,]):
        Rs = [None,]
        Rh = [None,]
        Tval = [None,]
        if self.T(Tval):
            Rh[0] = Tval[0]
            if self.R(Rh, Rs):
                Eval[0] = Rs[0]
                return 1
            return 0
        else:
            return 0

    def main(self):
        self.pos = 0
        self.exp = raw_input("Entre com a expressao a avaliar: ")
        self.exp += "#"
        valor = [None,]
        if self.E(valor):
            print "A expressao vale %s\n" % valor[0]
        else:
            print "Expressao invalida"

operacoes = Operacoes()


asdf = [None,]



