#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int pos=0;
char exp[20];
char temp[2] = {'T','0'};
char igual[1] = {'='};
char soma[1] = {'+'};

/* Exp -> dig R     {Exp.val = dig.val + R.val}
   R -> + dig R1 |   {R.val = dig.val + R1.val}
        vazio        {R.val = 0} */
int E(char Ecod[100], char Eplace[2]);
int T (char Tplace[2], char Tcod[100]);
int atrib();
   
char *gera_temp(char dio[]){
    temp[1] =  temp[1] + 1;
    strcat(dio, temp);
    return dio;
}

int atrib()
{
    char Ecod[100];
    char Eplace[2];
    char atribcod[100];
    char id[1];

    strcpy(Ecod, "");
    strcpy(Eplace, "");
    strcpy(atribcod, "");
    strcpy(id, "");

    printf("Atrib\n");
    if (!(exp[pos]>='0' && exp[pos]<='9') && (exp[pos] != '+') && (exp[pos] != '=')){ //rever teste de id
        printf("Achei id\n");
        id[0] = exp[pos];
        pos++;
        if (exp[pos] == '='){
            printf("Achei igual\n");
            pos++;
            if (E(Ecod, Eplace)){
                printf("Passei do E\n");                
                printf("Ecod == %s\n", Ecod);                
                strcat(atribcod, Ecod);
                printf("Concat Ecod\n");                
                strcat(atribcod, id); 
                printf("Concat id\n");                
                strcat(atribcod, igual);
                printf("Concat igual\n");                
                strcat(atribcod, Eplace);
                printf("Concat Eplace\n");                
                printf("%s", atribcod);
                return 1;
            }
        }
    }
}

int E(char Ecod[100], char Eplace[2]){
    char Tplace[2], Tcod[100];
    char Ecod1[100], Eplace1[2];
    strcpy(Tplace,"");
    strcpy(Tcod,"");
    strcpy(Ecod1,"");
    strcpy(Eplace1,"");
    printf("Entrei no E\n");
    if (T(Tplace, Tcod)){
        printf("Passei do T\n");
        if (exp[pos]=='+'){
            printf("Achei o +\n");
            pos++;
            if (E(Ecod1, Eplace1)){
                printf("Passei do E recursivo.\n");
                gera_temp(Eplace);
                strcat(Ecod, Tcod);
                strcat(Ecod, Ecod1);
                strcat(Ecod, Eplace);
                strcat(Ecod, igual);
                strcat(Ecod, Tplace);
                strcat(Ecod, soma);
                strcat(Ecod, Eplace1);
                return 1;
            }
            return 0;
        }
        else{
            Eplace = Tplace;
            return 1;
        }
    }
    return 0;
}

int T (char Tplace[2], char Tcod[100])
{
    int digval, R1val;
    char cte[1];
    strcpy(cte, "");
    printf("Entrei no T\n");
    if (exp[pos]!='+' && exp[pos]!='='){ //rever teste de id
        printf("Achei id ou cte\n");
        cte[0] = exp[pos];
        if (exp[pos]>='0' && exp[pos]<='9'){ //constante
            printf("Eh cte\n");
            gera_temp(Tplace);
            strcat(Tcod, Tplace);
            strcat(Tcod, igual); 
            strcat(Tcod, cte);
        }
        else{
            printf("Eh id\n");
            Tplace = cte;
            char vazio[1] = {' '};
            Tcod = vazio;
        }
        pos++;
        return 1;
    }
    return 0;
} 

int Exp (int *eval)
{
int Rval,digval;
if (exp[pos]>='0' && exp[pos]<='9')
   {
   digval=exp[pos]-'0';
   pos++;
   if (atrib()) 
      {
      
      return 1;
      }
   return 0;
   }
else return 0;
} 

int main()
{
int valor;
printf("Entre com a express�o a avaliar:");
fflush(stdin);
scanf("%s",exp);
if (atrib()){
    printf("\nDeu certo.");
    return 1;
}
printf("\nDeu errado.");
return 0;
}

